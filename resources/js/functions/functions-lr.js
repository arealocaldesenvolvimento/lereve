$(document).ready(function(){
    
    // toggle search
    $('._header .rightSection > .item.search > .searchButton').click(function(){

        $(this).toggleClass('active-content');
        $('._header .rightSection > .item.search > .searchcolumn').toggleClass('active-content');

    });

    // vitrine center mode
    $('.topsellers.no-slideshow .slideshow').slick({
        slidesToShow: 3,
        prevArrow: '<a class="slick-prev ui button icon"><i class="chevron left icon"></i></a>',
        nextArrow: '<a class="slick-next ui button icon"><i class="chevron right icon"></i></a>',
        arrows: true,
        dots: false,
        responsive: [
          {
            breakpoint: 768,
            settings: {
              slidesToShow: 1
            }
          },
          {
            breakpoint: 480,
            settings: {
              slidesToShow: 1
            }
          }
        ]
      });

    // vitrines get the look
    $('.getTheLook-type .slideshow').slick({
      prevArrow: '<a class="slick-prev ui mini button icon"><i class="chevron left icon"></i></a>',
      nextArrow: '<a class="slick-next ui mini button icon"><i class="chevron right icon"></i></a>',
      arrows: true,
      dots: false,
      slidesToShow: 2,
      rows: 2,
      infinite: false
  });

  // cards dropdown
  setTimeout(function(){
      $('.card.produto .content .info .sku-options.mask > .dropdown').dropdown();
  }, 2000);

  // botao voltar ao topo
  $("body").append("<a href='#' style='display: none;' class='scrollToTopButton'><span data-tooltip='Voltar ao topo' data-position='left center'></span></a>"), $(window).scroll(function() {
    $(this).scrollTop() > 100 ? $(".scrollToTopButton").fadeIn() : $(".scrollToTopButton").fadeOut()
  }), $(".scrollToTopButton").click(function() {
    return $("html, body").animate({
      scrollTop: 0
    }, 800), !1
  });

  // popup newsletter
  if (localStorage.news != 'ok') {	
      $('.ui.modal.news')
          .modal({
              onHidden    : function(){
                  localStorage.setItem('news', 'ok');
              },
              onApprove : function() {
                  $('.ui.modal.news').modal('hide');
                  $('button#btn_news').click();
                  localStorage.setItem('news');
              }
          })
          .modal('show')
      ;
  }

  $('.ui.modal.news .actions #emailNews').keyup(function(){
      $('.newsletter-bar #email_news').val($(this).val());

      if( $('#emailNews').val()=="" || $('#emailNews').val().indexOf('@')==-1 || $('#emailNews').val().indexOf('.')==-1 ) {
          $('.ui.modal.news .actions .button.approve').addClass('disabled');
      } else {
          $('.ui.modal.news .actions .button.approve').removeClass('disabled');
      }
      
  }); 

  // pp produto
  $('#openMedidas').click(function(){
      $('.ui.modal.tabelaDeMedidas').modal('show');
  });

  $('.boxAvaliacoes .column.boxes .button').click(function(){

    $('.boxAvaliacoes .column.form').slideToggle();

  });

});